# AfpDAQ
**This is meta repo to build AFP TDAQ and RCE code**
- `git clone --recurse-submodule <repo URL>`
- `source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-09-02-01`
- `cmake_config`
- `cd $CMTCONFIG`
- `make -j16`
- `make install`
